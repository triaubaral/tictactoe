<?php

abstract class Player{

	private $moves= array();
			
	function play ($move){
	
		array_push($this->moves, $move);
		
	}
	
	function path(){
		
		$str_move = "";
		foreach($this->moves as $move){
			
			$str_move .= $move;
			
		}
		
		return $str_move;
		
	}
	
	function getMoves(){
		return $this->moves;
	}
	
		
	
}

class PlayerX extends Player{
	
	function getSymbol(){
		return "X";
	}
	
}

class PlayerO extends Player{
	
	function getSymbol(){
		return "O";
	}
	
}

class BoardCoordinate{

 private $line;
 private $column;
 
 function __construct($line, $column){
	
	$this->line = $line;
	$this->column = $column; 
  }
 
 function getLine(){
   return $this->line;
 }
 
 function getColumn(){
	return $this->column;
 }
	
}

class Boardgame{

  private $board;
  public $coordinate;

  function __construct(){
	  
	$this->coordinate = array(
	"A1"=>new BoardCoordinate(0,0), "A2"=>new BoardCoordinate(0,1), "A3"=>new BoardCoordinate(0,2),
	"B1"=>new BoardCoordinate(1,0), "B2"=>new BoardCoordinate(1,1), "B3"=>new BoardCoordinate(1,2),
	"C1"=>new BoardCoordinate(2,0), "C2"=>new BoardCoordinate(2,1), "C3"=>new BoardCoordinate(2,2)
	);
	
	for($i=0;$i<3;$i++){
	  for($j=0; $j<3; $j++){
		$this->board[$i][$j]=" ";
	  }
	}

   }

  function getValue($line,$colum){
        return $this->board[$line][$colum];
  }

  function setValue($line, $colum, $value){

        if($this->validate($line,$colum,$value))
			$this->board[$line][$colum]=$value;
        else
			echo "coup invalide !!";
   }

   function validate($line,$col, $value){

	if($this->board[$line][$col] == $value)
		return false;

        return true;
   }

   function getBoard(){
	return $this->board;
   }

};

class TicTacToe{

  private $playerX;
  private $playerO;
  private $boardGame;
  private $boardGameView;
  
  private $victoryCombination = array( array("A1","A2","A3"), array("B1","B2","B3"), array("C1","C2","C3"), array("A1","B1","C1"), 
									  array("A2","B2", "C2"), array("A3","B3","C3"), array("A1","B2","C3"), array("C1","B2","A3"));
  
  
  
  function __construct(){
	
	$this->playerX = new PlayerX;
	$this->playerO = new PlayerO;
	$this->boardGame = new BoardGame;
	$this->boardGameView = new BoardGameVue;  
	  
  }
  
  function isMoveTwiceToSameCoordinate($move, $player){
	  
	   if(in_array($move, $player->getMoves())){
		  echo "You are not allowed to move twice to the same coordinate.".PHP_EOL;
		  return true;
	   }
	   
	   return false;
	  
   }
   
   function isMoveToAPlaceAlreadyOccupiedByOtherPlayer($move, $player){
	  
	  if(!strcmp($player->getSymbol(),"X")){
		  $otherPlayer = $this->playerO;
	  }
	  else{
		  $otherPlayer = $this->playerX;
	  }
	   
	  if(in_array($move, $otherPlayer->getMoves())){
			echo "You are not allowed to move to a place already occupied.".PHP_EOL;
			return true;  
	  }
	    
	  return false;
	   
   }
   
   function isMouvementExits($move){
	   
	  $validMoves = array_diff(array_keys($this->boardGame->coordinate), $this->playerX->getMoves(), $this->playerO->getMoves());
	  	  	  	  		
	  if(in_array($move, $validMoves)){		
	
		return true;
	  }
	  else{
		  
		 echo "Movements allowed are : ".implode(',',$validMoves).", please try again !".PHP_EOL;
	  }
	 
	  return false;
	   
	}
  
  function isValidMove($move,$player){
	 
		 return !$this->isMoveTwiceToSameCoordinate($move, $player)
		 && !$this->isMoveToAPlaceAlreadyOccupiedByOtherPlayer($move, $player)
		 && $this->isMouvementExits($move);	  	 
	 
  }
  
  function run(){
	  
	$player = $this->playerO;
	
	$i=0;
	  
	while(!$this->isWinner($player) && $i<9){
		
		 if(strcmp($player->getSymbol(),"X"))
			$player = $this->playerX;
		 else
		    $player = $this->playerO;
		    
		do{
	
		$move = readline(PHP_EOL."Player ".$player->getSymbol()."'s move (ex:A1) :");
		
		}
		while(!$this->isValidMove($move, $player));
		
		
		$player->play($move);
		
		$this->boardGame->setValue($this->boardGame->coordinate[$move]->getLine(), $this->boardGame->coordinate[$move]->getColumn(), $player->getSymbol());
		
		$this->boardGameView->display($this->boardGame->getBoard());
		
		$i++;
	}  
	
	
	if($i>=9)
		echo "No winners ! Try again :)".PHP_EOL;
	else	
		echo "Player ".$player->getSymbol()." has won the game !!".PHP_EOL;
	  
  }
  

  function isWinner($player){
	  
	 if(empty($player->getMoves()))
		return false;
		
	 echo "Player".$player->getSymbol()." path :".$player->path().PHP_EOL;
	 
	 
	 foreach($this->victoryCombination as $combination){
		 
		 $i=0;
		 foreach($player->getMoves() as $move){
		  	 
			 if(in_array($move, $combination))
			   $i++;
		 }
		 
		 if($i == 3){				
			return true;			 
		 }
		 
	 }

	return false;

  }

}


class BoardGameVue{

   private $boardgame;
   private $data;


   function __construct(){

	$this->boardgame="
		
	----TIC-TAC-TOE----

	**|1|***|2|***|3|**
	*     *     *     *
	A  a  *  b  *  c  *
	*     *     *     *
	*******************
	*     *     *     *
	B  d  *  e  *  f  *
	*     *     *     *
	*******************
	*     *     *     *
	C  g  *  h  *  i  *
	*     *     *     *
	*******************
	-------------------
	
	";

   }

   function display($tableau){

	$this->data=array(
	    "a"=>$tableau[0][0], "b"=>$tableau[0][1], "c"=>$tableau[0][2], 
	    "d"=>$tableau[1][0], "e"=>$tableau[1][1], "f"=>$tableau[1][2],
	    "g"=>$tableau[2][0], "h"=>$tableau[2][1], "i"=>$tableau[2][2]);

	$boardgame = $this->boardgame;

	foreach($this->data as $key=>$value){

		$boardgame=str_replace($key,$value,$boardgame);
	}

	print_r($boardgame);

   }
}

$tictactoe=new TicTacToe;

$tictactoe->run();
